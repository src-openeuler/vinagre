Name:           vinagre
Version:        3.22.0
Release:        14
Summary:        A remote desktop viewer for GNOME
License:        GPLv2+
URL:            https://wiki.gnome.org/Apps/Vinagre
Source0:        https://download.gnome.org/sources/%{name}/3.22/%{name}-%{version}.tar.xz

Patch0:         0001-handle-new-freerdp-pkgconfig-name.patch
Patch1:         0002-freerdp2-32bpp.patch
Patch2:         0003-fix-compilation-failed.patch
Patch3:         fix-appstream-data.patch

BuildRequires:  pkgconfig(avahi-gobject) pkgconfig(avahi-ui-gtk3) automake autoconf
BuildRequires:  pkgconfig(freerdp2) pkgconfig(gtk+-3.0) pkgconfig(gtk-vnc-2.0) pkgconfig(libsecret-1)
BuildRequires:  pkgconfig(libxml-2.0) pkgconfig(telepathy-glib) pkgconfig(vte-2.91) gnome-common
BuildRequires:  intltool libappstream-glib-devel itstool avahi-ui-gtk3 avahi-gobject
%ifarch %{ix86} x86_64
BuildRequires:  pkgconfig(spice-client-gtk-3.0)
%endif
Requires:       dbus telepathy-filesystem glib2 desktop-file-utils shared-mime-info

%description
This is Vinagre, a remote desktop viewer for the GNOME Desktop.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
ACLOCAL_FLAGS="$ACLOCAL_FLAGS" USE_GNOME2_MACROS=1 . gnome-autogen.sh
export CFLAGS="%{optflags} -Wno-deprecated-declarations -Wno-format-nonliteral"
%configure \
%ifarch %{ix86} x86_64
           --enable-spice \
%endif
           --enable-rdp --enable-ssh --with-avahi

%make_build

%install
%make_install
%find_lang vinagre --with-gnome

%check
make check

%files -f vinagre.lang
%license COPYING AUTHORS
%{_bindir}/%{name}
%{_datadir}/GConf/gsettings/*.convert
%{_datadir}/applications/*
%{_datadir}/dbus-1/services/*
%{_datadir}/glib*/schemas/*
%{_datadir}/icons/*/*/*/*
%{_datadir}/metainfo/*
%{_datadir}/mime/*/*
%{_datadir}/telepathy/*/*
%{_datadir}/%{name}/*

%files help
%doc NEWS README
%{_datadir}/man/man1/*

%changelog
* Fri Feb 03 2023 yaoxin <yaoxin30@h-partners.com> - 3.22.0-14
- Fix complication failed

* Mon Aug 2 2021 Haiwei Li <lihaiwei8@huawei.com> - 3.22.0-13
- Fix complication failed due to gcc upgrade

* Thu Feb 13 2020 daiqianwen <daiqianwen@huawei.com> - 3.22.0-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add buildrequires

* Thu Nov 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.22.0-11
- Package init
